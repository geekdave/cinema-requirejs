/* global define */
define(['jquery', 'mockjax'], function($) {
	'use strict';

	/**
	 * This function is used to trigger mocking. It should be called before any AJAX request.
	 * @return {function}
	 */
	var mock = function() {

		$.ajaxSetup({
			dataType: 'json'
		});

		$.mockjax(function(settings) {

          if (settings.url.match(/api\/movies/)) {

            return {
              proxy: 'scripts/jsons/movies.json'
            };
          }
        });
	};

	return {
		start: mock
	};
});
