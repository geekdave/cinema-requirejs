/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var DetailsView = Backbone.View.extend({

        template: JST['app/scripts/templates/details.ejs'],

        render: function() {
          var tmpl = this.template;
          this.$el.html(tmpl(this.model.toJSON()));
          return this;
        }
    });

    return DetailsView;
});
